package com.company;

import java.util.Arrays;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Sweets[] arraySweets = new Sweets[15];
        arraySweets[0] = new Sweets("Milka Bubbles", 100, 46,700);
        arraySweets[1] = new Sweets("Kazakhstan", 20, 13, 80);
        arraySweets[2] = new Sweets("Kiss-kiss", 7, 5, 5);
        arraySweets[3] = new Sweets("Mishki gammi", 100, 40, 320);
        arraySweets[4] = new Sweets("Rochen wafers", 216, 100 ,379);
        arraySweets[5] = new Sweets("Kinder surprise", 20, 9, 375);
        arraySweets[6] = new Sweets("Albeni", 320, 140, 800);
        arraySweets[7] = new Sweets("Snickers", 50, 26, 195);
        arraySweets[8] = new Sweets("Mars", 51, 32,195);
        arraySweets[9] = new Sweets("M&M", 90, 24, 220);
        arraySweets[10] = new Sweets("Twix", 75, 45, 195);
        arraySweets[11] = new Sweets("Bounty", 57, 14, 195);
        arraySweets[12] = new Sweets("Chupa-Chups", 6, 4, 20);
        arraySweets[13] = new Sweets("Luna", 40, 15, 145);
        arraySweets[14] = new Sweets("Barni", 30, 17, 129);


        Arrays.sort(arraySweets);

        System.out.println("There are 15 sweets in your kulek. They are sorted based on the weight: ");
        for (Sweets h: arraySweets) {
            System.out.println(h);
        }
        System.out.println("----------------------------------------------------------------------------------");

        //Calculate total weight and price
        float totalWeight = 0;
        int totalPrice = 0;
        for (int i=0; i<15; i++) {
            totalWeight = arraySweets[i].calculateWeight(totalWeight);
            totalPrice = arraySweets[i].calculatePrice(totalPrice);
        }
        System.out.println("The weight of your kulek is: " + totalWeight + " grams");
        System.out.println("The price of your kulek is: " + totalPrice + " tenge");
        System.out.println("----------------------------------------------------------------------------------");

        //Find sweet which satisfy given sugar content range
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter the range for the sugar content (in %) of the sweet (from 24,5% to 71,43%):");
        float sugarMin = scanner.nextFloat();
        float sugarMax = scanner.nextFloat();
        int countSugar = 0;
        for (int i=0; i<15; i++) {
            countSugar = countSugar + arraySweets[i].countingSC(sugarMin, sugarMax);
        }
        System.out.println("There are " + countSugar + " sweets which sugar content is from " + sugarMin + " to " + sugarMax + "%. ");
        if (countSugar > 0) {
            System.out.println("They are: ");
            for (int i=0; i<15; i++) {
                arraySweets[i].findSugar(sugarMin, sugarMax);
            }
        }
        System.out.println("-----------------------------------------------------------------------------------");
    }
}
