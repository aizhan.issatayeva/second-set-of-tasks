package com.company;

import java.util.Arrays;

public class Sweets implements Comparable<Sweets> {
    String name;
    float weight;   // in grams
    float sugar;    // in grams
    int price;    // in KZT

    public Sweets(String n, float w, float s, int p) {
        this.name = n;
        this.weight = w;
        this.sugar = s;
        this.price = p;
    }

    public float getWeight() {
        return weight;
    }


    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Name=").append(name);
        sb.append(", weight=").append(weight);
        sb.append(", sugar weight=").append(sugar);
        sb.append(", price=").append(price);
        return sb.toString();
    }

    public int compareTo(Sweets o) {
        return (int) (this.getWeight() - o.getWeight());
    }

    public float calculateWeight(float s) {
        float sum;
        sum = s + weight;
        return sum;
    }

    public int calculatePrice(int s) {
        int sum;
        sum = s + price;
        return sum;
    }


    public int countingSC(float min, float max) {
        int count = 0;
        if (sugar*100/weight >= min && sugar*100/weight <= max) {
            count = 1;
        }
        return count;
    }


    public void findSugar(float min, float max) {
        if (sugar*100/weight >= min && sugar*100/weight <= max) {
            double s = sugar*100/weight;
            System.out.print(name);
            System.out.printf(" consists of %.2f", s);
            System.out.println("% of sugar.");
        }
    }
}
