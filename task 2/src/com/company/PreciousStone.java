package com.company;

public class PreciousStone extends Stone implements Comparable<Stone>{

    String name;
    double weight;
    double pricePercarat;
    double transparency;

    public PreciousStone(String n, double w, double ppc, double t) {
        name = n;
        weight = w;
        pricePercarat = ppc;
        transparency = t;
    }

    public double getPricePercarat() {
        return pricePercarat;
    }

    public double getWeight() {
        return weight;
    }

    public double getPrice() {
        return pricePercarat*weight;
    }

    public double getTransparency() {
        return transparency;
    }
    public boolean inTheRange(double min, double max) {
        if (this.getTransparency() >= min/100 && this.getTransparency() <= max/100) {
            return true;
        } else {
            return false;
        }
    }

    public void PrintStones(boolean check) {
        if (check) {
            System.out.println(this.name + " has a transparency of " + this.transparency*100 + "%.");
        }
    }

    @Override
    public int compareTo(Stone anotherStone) {
        return (int)(this.getPricePercarat() - anotherStone.getPricePercarat());
    }

    public String toString() {
        final StringBuffer sb = new StringBuffer("Name=").append(name);
        sb.append(", weight=").append(weight);
        sb.append(" grams, price per carat=").append(pricePercarat);
        sb.append(", transparency=").append(transparency*100);
        sb.append("%");
        return sb.toString();
    }

}