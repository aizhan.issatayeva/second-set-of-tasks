package com.company;

import java.util.Scanner;
import java.util.TreeSet;

public class Main {

    public static void main(String[] args) {
        // Task 1 and 4
        TreeSet<Stone> stoneList = new TreeSet<Stone>();
        double totalPreciousWeight = 0, totalPreciousPrice = 0;
        double totalSemiPreciousWeight = 0, totalSemiPreciousPrice = 0;

        stoneList.add(new PreciousStone("Diamond", 1.2, 1190000, 0.9));
        stoneList.add(new PreciousStone("Emerald", 1.6, 305000, 0.82));
        stoneList.add(new PreciousStone("Ruby", 1.4, 1180000, 0.75));
        stoneList.add(new PreciousStone("Jadeite", 1, 3000000, 0.8));

        stoneList.add(new SemiPreciousStone("Black Opal", 4, 9500, 0.5));
        stoneList.add(new SemiPreciousStone("Musgravite", 2, 35000, 0.61));
        stoneList.add(new SemiPreciousStone("Red Beryl", 3, 10000, 0.52));
        stoneList.add(new SemiPreciousStone("Tanzanite", 5, 1200, 0.45));


        System.out.println("There are the following precious stones, sorted based on the price per carat");
        for (Stone stone : stoneList) {
            if (stone instanceof PreciousStone) {
                System.out.println(stone);
            }
        }
        System.out.println();
        System.out.println("There are the following semi-precious stones, sorted based on the price per carat: ");
        for (Stone b : stoneList) {
            if (b instanceof SemiPreciousStone) {
                System.out.println(b);
            }
        }
        System.out.println("------------------------------------------------------------");

        //Task 2 and 3
        for (Stone stone : stoneList)
            if (stone instanceof PreciousStone) {
                totalPreciousWeight += stone.getWeight();
                totalPreciousPrice += stone.getPrice();
            } else if (stone instanceof SemiPreciousStone) {
                totalSemiPreciousWeight += stone.getWeight();
                totalSemiPreciousPrice += stone.getPrice();
            }

        System.out.println("Necklace constructed from precious stones:");
        System.out.printf("Total weight: %.2f grams", totalPreciousWeight);
        System.out.println();
        System.out.printf("Total price: $%.2f", totalPreciousPrice);
        System.out.println("\n");
        System.out.println("Necklace constructed from semi-precious stones:");
        System.out.printf("Total weight: %.2f grams", totalSemiPreciousWeight);
        System.out.println();
        System.out.printf("Total price: $%.2f", totalSemiPreciousPrice);
        System.out.println();

        System.out.println("-----------------------------------------------------------------------------------");

        //Find sweet which satisfy given sugar content range
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter the range of transparency (in %) of the stone (from 45% to 90%):");
        double transpMin = scanner.nextDouble();
        double transpMax = scanner.nextDouble();
        int countTransp = 0;
        for (Stone stone : stoneList) {
            if (stone.inTheRange(transpMin, transpMax)) {
                countTransp++;
            }
        }
        System.out.println("There are " + countTransp + " stones which transparency is in the range " + transpMin + "% to " + transpMax + "%: ");
        for (Stone stone : stoneList) {
            stone.PrintStones(stone.inTheRange(transpMin, transpMax));
        }


        System.out.println("-----------------------------------------------------------------------------------");

    }
}